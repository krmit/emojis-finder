import { assert } from "chai";
import { CreateEmojiData } from "../src/storage";

describe("Test of testing framework", () => {
  describe("Basic tests", () => {
    it("Is something is a string", () => {
      assert.typeOf("hello", "string");
    });
    it("Is something is a number", () => {
      assert.typeOf(1, "number");
    });
  });
  describe("Test of deep equal", () => {
    it("Is this arrays equal", () => {
      assert.deepEqual(["a", "b", "c"], ["a", "b", "c"]);
    });
    it("Is this object equal", () => {
      assert.deepEqual({ a: 1, b: 2 }, { a: 1, b: 2 });
    });
  });
});

describe("Test of storage", () => {
  describe("Basic tests", () => {
    it("Test a simpel emoji data", () => {
      const result = CreateEmojiData([
        {
          emoji: "👻",
          keywords: ["ghost", "spöke"],
        },
      ]);
      assert.deepEqual(result, { ghost: "👻", g: "👻", s: "👻", spöke: "👻" });
    });
  });
});
