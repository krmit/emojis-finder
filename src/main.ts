#!/usr/bin/env node
import { EmojiStorage } from "./storage";
import path from "path";

async function main() {
  const keywords: string[] = process.argv;
  keywords.shift();
  keywords.shift();

  try {
    let data_path = path.normalize(
      path.join(__dirname, "..", "data", "emojis.json")
    );
    const index = await EmojiStorage(data_path);

    let emojis: string[] = [];
    for (const keyword of keywords) {
      let emoji = index[keyword];
      if (emoji === undefined) {
        emojis.push("No emoji");
      } else {
        emojis.push(emoji);
      }
    }
    console.log(emojis.join(" "));
  } catch (e) {
    console.error("Can not find data file.");
  }
}

main();
