import fs from "fs-extra";

/**
 * Creat a object with emojis from data file
 *
 * @param dataPath - A path to json file with data.
 * @returns - A object with emojis information.
 */
export async function EmojiStorage(dataPath: string) {
  let data;
  data = await fs.readFile(dataPath, "utf-8").then(JSON.parse);

  return CreateEmojiData(data);
}

/**
 * Creat a object with emojis from a JSON object.
 *
 * @param data - JSON object.
 * @returns - A object with emojis information.
 */
export function CreateEmojiData(data: any[]) {
  const index: { [key in string]: string } = {};

  for (const emoji of data) {
    for (const keyword of emoji.keywords) {
      index[keyword] = emoji.emoji;
      index[keyword[0]] = emoji.emoji;
    }
  }

  return index;
}
