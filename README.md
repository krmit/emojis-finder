# emojis-finder

## Install

```
$ npm install
```

## Usage

```

```

## License

GPL version 3, see the file COPYING.

## Contribute

Please feel free to open an issue or submit a pull request.

## Changelog

- **0.0.1** _2022-12-05_ First version published

## Author

**©Magnus Kronnäs 2022 [magnus.kronnas.se](https://magnus.kronnas.se)**
